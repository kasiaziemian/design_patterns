from functools import wraps

def make_blink(function):
    @wraps(function)
    def decorator():
        ret = function()
        return 'blink' + ret
    return decorator

@make_blink
def test():
    return "test"

print(test())

print(test.__name__)
print(test.__doc__)